package com.dpoddubko.app;

import android.app.Application;

import com.dpoddubko.app.di.AppComponent;
import com.dpoddubko.app.di.AppModule;
import com.dpoddubko.app.di.DaggerAppComponent;
import com.dpoddubko.main.di.MainComponent;
import com.dpoddubko.main.di.MainModule;
import com.dpoddubko.navigation.NavigationModule;
import com.dpoddubko.network.NetworkModule;
import com.google.firebase.FirebaseApp;

public class App extends Application {

    private AppComponent appComponent;
    private MainComponent mainComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        FirebaseApp.initializeApp(this);
        appComponent = createAppComponent();
    }


    private AppComponent createAppComponent() {
        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .networkModule(new NetworkModule())
                .navigationModule(new NavigationModule())
                .build();
    }

    public MainComponent createMainComponent() {
        mainComponent = appComponent.plus(new MainModule());
        return mainComponent;
    }

    public void releaseMainComponent() {
        mainComponent = null;
    }

    public MainComponent getMainComponent() {
        return mainComponent;
    }
}
