package com.dpoddubko.app.di;

import android.app.Application;
import android.content.Context;

import com.dpoddubko.network.ResponseCacheService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Context context;

    public AppModule(Application application) {
        context = application;
    }

    @Provides
    @Singleton
    Context provideContext() {
        return context;
    }

    @Provides
    @Singleton
    ResponseCacheService provideCacheService() {
        return new ResponseCacheService();
    }
}
