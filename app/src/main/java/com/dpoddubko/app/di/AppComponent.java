package com.dpoddubko.app.di;

import com.dpoddubko.main.di.MainComponent;
import com.dpoddubko.main.di.MainModule;
import com.dpoddubko.navigation.NavigationModule;
import com.dpoddubko.network.NetworkModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
        AppModule.class,
        NetworkModule.class,
        NavigationModule.class})

public interface AppComponent {
    MainComponent plus(MainModule mainModule);
}
