package com.dpoddubko.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.dpoddubko.R;

import org.apache.commons.lang3.StringUtils;

import static com.dpoddubko.utils.Constants.ERROR_ACTION;
import static com.dpoddubko.utils.Constants.ERROR_MSG_EXTRA;
import static com.dpoddubko.utils.Constants.LOCATION_CHANGED_ACTION;
import static com.dpoddubko.utils.Constants.LOCATION_EXTRA;

public class LocationService extends Service {

    private static final String TAG = LocationService.class.getName();
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 10f;

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override

        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: getLatitude = " + location.getLatitude());
            sendMessage(location);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
            sendErrorMessage(String.format(getString(R.string.location_disabled),
                    StringUtils.capitalize(provider)));
            stopSelf();
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }

        private void sendMessage(Location location) {
            Log.d("sender", "Broadcasting message");
            Intent intent = new Intent(LOCATION_CHANGED_ACTION);
            intent.putExtra(LOCATION_EXTRA, location);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            stopSelf();
        }

        private void sendErrorMessage(String errorMessage) {
            Log.d("sender", "Broadcasting message");
            Intent intent = new Intent(ERROR_ACTION);
            intent.putExtra(ERROR_MSG_EXTRA, errorMessage);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
            stopSelf();
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand");
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        initializeLocationManager();
        try {
            mLocationManager.requestLocationUpdates(
                    LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[0]);

            mLocationManager.requestLocationUpdates(
                    LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                    mLocationListeners[1]);
        } catch (SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "network provider does not exist, " + ex.getMessage());
        }
    }

    @Override
    public void onDestroy() {
        Log.e(TAG, "onDestroy");
        super.onDestroy();
        if (mLocationManager != null) {
            for (int i = 0; i < mLocationListeners.length; i++) {
                try {
                    mLocationManager.removeUpdates(mLocationListeners[i]);
                } catch (Exception ex) {
                    Log.i(TAG, "fail to remove location listners, ignore", ex);
                }
            }
        }
    }

    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext()
                    .getSystemService(Context.LOCATION_SERVICE);
        }
    }
}