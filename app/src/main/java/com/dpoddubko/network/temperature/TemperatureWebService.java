package com.dpoddubko.network.temperature;

import com.dpoddubko.network.temperature.model.TempData;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TemperatureWebService {

    @GET("weather")
    Observable<TempData> getTemp(
            @Query("lat") double lat,
            @Query("lon") double lng);
}
