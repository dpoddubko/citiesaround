package com.dpoddubko.network;

import android.support.v4.util.LruCache;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class ResponseCacheService {

    private LruCache<Class<?>, Single<?>> apiObservables;

    public ResponseCacheService() {
        apiObservables = new LruCache<>(5);
    }

    public Single<?> getPreparedObservable(Single<?> unPreparedObservable, Class<?> clazz,
                                           boolean cacheObservable, boolean useCache) {

        Single<?> preparedObservable = null;
        if (useCache)
            preparedObservable = apiObservables.get(clazz);
        if (preparedObservable != null)
            return preparedObservable;
        preparedObservable = unPreparedObservable
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
        if (cacheObservable) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }
        return preparedObservable;
    }

    public void clearCache() {
        apiObservables.evictAll();
    }
}
