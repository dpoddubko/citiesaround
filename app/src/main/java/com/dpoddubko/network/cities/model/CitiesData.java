package com.dpoddubko.network.cities.model;

import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "cities", strict = false)
public class CitiesData {

    @ElementList(name = "city", inline = true)
    private List<CityData> city;

    public List<CityData> getCity() {
        return city;
    }

    public void setCity(List<CityData> city) {
        this.city = city;
    }

    @Override
    public String toString() {
        return "Cities{" +
                "city=" + city +
                '}';
    }
}
