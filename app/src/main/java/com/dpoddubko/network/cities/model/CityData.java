package com.dpoddubko.network.cities.model;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "city", strict = false)
public class CityData {

    @Attribute
    private String name;

    @Attribute
    private double dist;

    @Attribute
    private double lat;

    @Attribute
    private double lng;

    @Attribute(required = false)
    private String population;

    private double temperature;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDist() {
        return dist;
    }

    public void setDist(double dist) {
        this.dist = dist;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getPopulation() {
        return population;
    }

    public void setPopulation(String population) {
        this.population = population;
    }

    public double getTemperature() {
        return temperature;
    }

    public void setTemperature(double temperature) {
        this.temperature = temperature;
    }

    @Override
    public String toString() {
        return "CityData{" +
                "lng='" + lng + '\'' +
                ", name='" + name + '\'' +
                ", dist='" + dist + '\'' +
                ", lat='" + lat + '\'' +
                ", population='" + population + '\'' +
                '}';
    }

}
