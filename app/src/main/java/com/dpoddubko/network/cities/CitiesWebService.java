package com.dpoddubko.network.cities;

import com.dpoddubko.network.cities.model.CitiesData;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface CitiesWebService {
    @Headers("Referer: https://www.freemaptools.com/find-cities-and-townsinside-radius.htm")
    @GET("get-all-cities-inside.php")
    Single<CitiesData> getCities(@Query("lat") double lat,
                                 @Query("lng") double lng,
                                 @Query("radius") int radius);
}
