package com.dpoddubko.network;

import com.dpoddubko.network.cities.CitiesWebService;
import com.dpoddubko.network.temperature.RequestInterceptor;
import com.dpoddubko.network.temperature.TemperatureWebService;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

@Module
public class NetworkModule {
    public static final int CONNECT_TIMEOUT_IN_MS = 30000;
    private static final String BASE_CITY_URL = "https://www.freemaptools.com/ajax/";
    private static final String BASE_TEMP_URL = "https://samples.openweathermap.org/data/2.5/";

    @Provides
    @Singleton
    Interceptor requestInterceptor(RequestInterceptor interceptor) {
        return interceptor;
    }

    @Provides
    @Singleton
    HttpLoggingInterceptor provideLoggingInterceptor() {
        return new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
    }

    @Provides
    @Singleton
    @Named("OkHttpClientForTemp")
    OkHttpClient provideTempOkHttpClient(HttpLoggingInterceptor loggingInterceptor,Interceptor requestInterceptor) {
        return new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                .addInterceptor(loggingInterceptor)
                .addInterceptor(requestInterceptor)
                .build();
    }

    @Provides
    @Singleton
    @Named("OkHttpClientForCity")
    OkHttpClient provideCitiesOkHttpClient(HttpLoggingInterceptor loggingInterceptor) {
        return new OkHttpClient.Builder()
                .connectTimeout(CONNECT_TIMEOUT_IN_MS, TimeUnit.MILLISECONDS)
                .addInterceptor(loggingInterceptor)
                .build();
    }


    @Singleton
    @Provides
    @Named("RetrofitForCity")
    Retrofit retrofitForCity(@Named("OkHttpClientForCity")OkHttpClient okHttpClient) {
        return new Retrofit
                .Builder()
                .baseUrl(BASE_CITY_URL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Singleton
    @Provides
    CitiesWebService cityWebService(@Named("RetrofitForCity") Retrofit retrofit) {
        return retrofit.create(CitiesWebService.class);
    }

    @Singleton
    @Provides
    @Named("RetrofitForTemperature")
    Retrofit retrofitForTemperature(@Named("OkHttpClientForTemp") OkHttpClient okHttpClient) {
        return new Retrofit
                .Builder()
                .baseUrl(BASE_TEMP_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(okHttpClient)
                .build();
    }

    @Singleton
    @Provides
    TemperatureWebService temperatureWebService(@Named("RetrofitForTemperature") Retrofit retrofit) {
        return retrofit.create(TemperatureWebService.class);
    }

}
