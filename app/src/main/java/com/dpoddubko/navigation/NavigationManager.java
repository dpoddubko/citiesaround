package com.dpoddubko.navigation;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.dpoddubko.R;

public class NavigationManager {

    private FragmentManager mFragmentManager;

    public void init(FragmentManager fragmentManager) {
        mFragmentManager = fragmentManager;
    }

    public void replace(Fragment fragment) {

        if (mFragmentManager != null) {
            mFragmentManager.beginTransaction()
                    .replace(R.id.container, fragment, fragment.getClass().getName())
                    .setCustomAnimations(R.anim.slide_in_left,
                            R.anim.slide_out_left,
                            R.anim.slide_out_right,
                            R.anim.slide_in_right)
                    .addToBackStack(fragment.getClass().getName())
                    .commit();
        }
    }

    public boolean isRootFragmentAlreadySet() {
        return mFragmentManager.getBackStackEntryCount() > 0;
    }

    public void navigateBack(Activity baseActivity) {
        if (mFragmentManager.getBackStackEntryCount() == 0) {
            baseActivity.finish();
        }
    }
}
