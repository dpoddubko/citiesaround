package com.dpoddubko.navigation;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class NavigationModule {
    @Provides
    @Singleton
    NavigationManager provideNavigationManager() {
        return new NavigationManager();
    }
}
