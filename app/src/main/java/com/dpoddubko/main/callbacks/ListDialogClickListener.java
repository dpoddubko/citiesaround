package com.dpoddubko.main.callbacks;

public interface ListDialogClickListener {

    void onSortByName();

    void onSortRangeAsc();

    void onSortRangeDesc();
}