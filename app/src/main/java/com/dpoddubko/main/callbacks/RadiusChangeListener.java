package com.dpoddubko.main.callbacks;

public interface RadiusChangeListener {
    void onRadiusChanged(int radius);
}