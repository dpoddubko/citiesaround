package com.dpoddubko.main;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import com.dpoddubko.R;
import com.dpoddubko.app.App;
import com.dpoddubko.main.view.MainFragment;
import com.dpoddubko.navigation.NavigationManager;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity {

    @Inject
    NavigationManager navigator;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getApplication()).createMainComponent().inject(this);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        navigator.init(getSupportFragmentManager());
        if (!navigator.isRootFragmentAlreadySet()) {
            navigator.replace(MainFragment.newInstance());
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((App) getApplication()).releaseMainComponent();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        navigator.navigateBack(this);
    }
}