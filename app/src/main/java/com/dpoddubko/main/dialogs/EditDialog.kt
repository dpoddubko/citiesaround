package com.dpoddubko.dialogtest

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import com.dpoddubko.R
import com.dpoddubko.main.callbacks.RadiusChangeListener
import kotlinx.android.synthetic.main.edit_dialog_layout.view.*
import org.jetbrains.anko.noButton
import org.jetbrains.anko.support.v4.alert
import org.jetbrains.anko.support.v4.toast
import org.jetbrains.anko.yesButton

@SuppressLint("ValidFragment")
class EditDialog(private val radiusChangeListener: RadiusChangeListener) : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): AlertDialog {
        val builder = AlertDialog.Builder(activity!!)
        val v = activity?.layoutInflater?.inflate(R.layout.edit_dialog_layout, null)
        val edit = v?.edit_dialog_change_radius
        builder.setView(v)

        v?.button_ok?.setOnClickListener {
            val radius = edit?.text.toString()
            if (radius.isBlank()) {
                edit?.error = activity?.getString(R.string.blank_radius)
            } else {
                radiusChangeListener.onRadiusChanged(Integer.valueOf(radius))
                dismiss()
            }
        }

        v?.button_cancel?.setOnClickListener {
            dismiss()
        }



        return builder.create()
    }
}
