package com.dpoddubko.main.dialogs

import android.content.Context
import org.jetbrains.anko.alert
import org.jetbrains.anko.yesButton

fun showDialog(context: Context, msg: String, title: String) {
    context.alert(msg, title) {
        this.iconResource = android.R.drawable.ic_dialog_alert
        yesButton {}
    }.show()
}