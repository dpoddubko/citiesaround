package com.dpoddubko.main.interactor;

import com.dpoddubko.main.interactor.model.City;
import com.dpoddubko.network.cities.CitiesWebService;

import java.util.List;

import io.reactivex.Single;

public class MainInteractorImpl implements MainInteractor {

    private CitiesWebService citiesWebService;


    public MainInteractorImpl(CitiesWebService citiesWebService) {
        this.citiesWebService = citiesWebService;
    }

    @Override
    public Single<List<City>> getCities(double lat, double lng, int radius) {
        return citiesWebService.getCities(lat, lng, radius)
                .map(CityMaper::transform);
    }
}
