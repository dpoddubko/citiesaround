package com.dpoddubko.main.interactor.model;

import android.os.Parcel;
import android.os.Parcelable;

public class City implements Parcelable {

    private String name;
    private double lat;
    private double lng;
    private double distance;
    private String temperature;
    private boolean isFirst;
    private DistanceType distanceType;

    public City(String name,
                double lat,
                double lng,
                double distance,
                String temperature,
                boolean isFirst) {

        this.name = name;
        this.lat = lat;
        this.lng = lng;
        this.distance = distance;
        this.temperature = temperature;
        this.isFirst = isFirst;
    }

    public City(Parcel in) {
        name = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
        temperature = in.readString();
    }

    public static final Creator<City> CREATOR = new Creator<City>() {
        @Override
        public City createFromParcel(Parcel in) {
            return new City(in);
        }

        @Override
        public City[] newArray(int size) {
            return new City[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public boolean isFirst() {
        return isFirst;
    }

    public void setFirst(boolean first) {
        isFirst = first;
    }

    public City() {
    }

    public DistanceType getDistanceType() {
        return distanceType;
    }

    public void setDistanceType(DistanceType distanceType) {
        this.distanceType = distanceType;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeDouble(lat);
        parcel.writeDouble(lng);
        parcel.writeString(temperature);
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", distance=" + distance +
                ", temperature='" + temperature + '\'' +
                '}';
    }
}
