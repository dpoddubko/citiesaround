package com.dpoddubko.main.interactor;

import com.dpoddubko.main.interactor.model.City;
import com.dpoddubko.network.cities.model.CitiesData;
import com.dpoddubko.network.cities.model.CityData;

import java.util.ArrayList;
import java.util.List;

public class CityMaper {

    static List<City> transform(CitiesData citiesData) {
        List<City> result = new ArrayList<>();
        for (CityData c : citiesData.getCity()) {
            result.add(new City(
                    c.getName(),
                    c.getLat(),
                    c.getLng(),
                    c.getDist(),
                    "",
                    false
            ));
        }
        return result;
    }
}
