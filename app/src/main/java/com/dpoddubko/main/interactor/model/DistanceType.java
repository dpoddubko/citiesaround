package com.dpoddubko.main.interactor.model;

public enum DistanceType {

    NEAR("Near"), FAR("Far"), VERY_FAR("Very far");

    public final String typeName;

    DistanceType(String typeName) {
        this.typeName = typeName;
    }
}
