package com.dpoddubko.main.interactor;

import com.dpoddubko.main.interactor.model.City;

import java.util.List;

import io.reactivex.Single;

public interface MainInteractor {

    Single<List<City>> getCities(double lat, double lng, int radius);

}
