package com.dpoddubko.main.view;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dpoddubko.R;
import com.dpoddubko.app.App;

import butterknife.ButterKnife;
import butterknife.Unbinder;

import static com.dpoddubko.main.dialogs.DialogsKt.showDialog;
import static com.dpoddubko.utils.Constants.ERROR_ACTION;
import static com.dpoddubko.utils.Constants.ERROR_MSG_EXTRA;
import static com.dpoddubko.utils.Constants.LOCATION_CHANGED_ACTION;
import static com.dpoddubko.utils.Constants.LOCATION_EXTRA;

public abstract class BaseFragment extends Fragment {

    private static final int LOCATION_REQUEST = 111;
    private Unbinder unbinder;
    private View layout;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (getLayoutResId() != 0)
            return inflater.inflate(getLayoutResId(), container, false);
        else
            return super.onCreateView(inflater, container, savedInstanceState);
    }

    public View getLayout() {
        return layout;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        layout = view;
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
    }

    protected void requestPermissions() {
        if (ActivityCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getActivity(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {
            requestExternalStoragePermissions();
        } else {
            updateLocation();
        }
    }

    private void requestExternalStoragePermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            Snackbar.make(layout, R.string.location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, view -> ActivityCompat.requestPermissions(getActivity(),
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION},
                            LOCATION_REQUEST)).show();
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION},
                    LOCATION_REQUEST);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        boolean granted = true;
        if (requestCode == LOCATION_REQUEST) {
            if (grantResults.length > 0) {
                for (int result : grantResults) {
                    if (result != PackageManager.PERMISSION_GRANTED) {
                        granted = false;
                    }
                }
            } else {
                granted = false;
            }
            if (granted) {
                Snackbar.make(layout, R.string.permission_available,
                        Snackbar.LENGTH_SHORT).show();
            } else {
                Snackbar.make(layout, R.string.permissions_not_granted,
                        Snackbar.LENGTH_SHORT).show();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
        ((App) getActivity().getApplication()).releaseMainComponent();
    }

    private BroadcastReceiver locationReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(LOCATION_CHANGED_ACTION)) {
                Location location = intent.getParcelableExtra(LOCATION_EXTRA);
                hideProgress();
                setLocation(location);
                Snackbar.make(layout, R.string.service_stopped, Snackbar.LENGTH_SHORT).show();
            } else {
                hideProgress();
                showDialog(getContext(), getString(R.string.location_disabled_title),
                        intent.getStringExtra(ERROR_MSG_EXTRA));
            }
        }
    };

    @Override
    public void onPause() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
                locationReceiver);
        super.onPause();
    }

    @Override
    public void onResume() {
        LocalBroadcastManager manager = LocalBroadcastManager.getInstance(getActivity());
        manager.registerReceiver(locationReceiver, new IntentFilter(LOCATION_CHANGED_ACTION));
        manager.registerReceiver(locationReceiver, new IntentFilter(ERROR_ACTION));
        super.onResume();
    }

    protected abstract void updateLocation();

    protected abstract int getLayoutResId();

    protected abstract void setLocation(Location location);

    protected abstract void hideProgress();
}
