package com.dpoddubko.main.view;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.dpoddubko.R;
import com.dpoddubko.app.App;
import com.dpoddubko.dialogtest.EditDialog;
import com.dpoddubko.main.adapter.view.MainAdapter;
import com.dpoddubko.main.interactor.model.City;
import com.dpoddubko.main.presenter.MainPresenter;
import com.dpoddubko.map.MapsFragment;
import com.dpoddubko.navigation.NavigationManager;
import com.dpoddubko.service.LocationService;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.dpoddubko.main.dialogs.DialogsKt.showDialog;
import static com.dpoddubko.utils.InternetUtils.isInternetAvailable;
import static com.dpoddubko.utils.LocationUtils.isLocationEquals;

public class MainFragment extends BaseFragment implements MainView {

    public static final String LOCATION_BUNDLE = "location_bundle";

    @Inject
    MainPresenter mainPresenter;

    @Inject
    NavigationManager navigator;

    private MainAdapter adapter;

    @BindView(R.id.main_fragment_progress_bar)
    FrameLayout progress;

    @BindView(R.id.tap_to_load_title)
    TextView tapMessage;

    private Bundle savedState;
    private FloatingActionButton fab;
    private Location location;

    public MainFragment() {
        // Required empty public constructor
    }

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ((App) getActivity().getApplication()).createMainComponent().inject(this);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mainPresenter.setView(this);

        getLocationFromBundle(savedInstanceState);

        adapter = new MainAdapter(Collections.emptyList());

        fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(view1 -> {
            if (isInternetAvailable(getContext())) {
                requestPermissions();
            } else {
                showDialog(getContext(), getString(R.string.internet_disabled_title),
                        getString(R.string.check_internet));
            }
        });
        RecyclerView recyclerView = view.findViewById(R.id.city_recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);

        setHasOptionsMenu(adapter.hasCities());
        needToShowMessage();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != VISIBLE) {
                    fab.show();
                }
            }
        });
    }

    private void getLocationFromBundle(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null && savedState == null) {
            savedState = savedInstanceState;
        }
        if (savedState != null) {
            location = savedState.getParcelable(LOCATION_BUNDLE);
        }
        savedState = null;
    }

    private void needToShowMessage() {
        if (!adapter.hasCities()) {
            tapMessage.setVisibility(VISIBLE);
        } else {
            tapMessage.setVisibility(GONE);
        }
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.fragment_main;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mainPresenter.destroy();
        savedState = saveState();
    }

    private Bundle saveState() {
        Bundle state = new Bundle();
        if (location != null)
            state.putParcelable(LOCATION_BUNDLE, location);
        return state;
    }

    @Override
    public void updateCity(List<City> data) {
        adapter.updateList(data);
    }

    @Override
    public Context getViewContext() {
        return getActivity().getApplication().getApplicationContext();
    }

    @Override
    public void showProgress() {
        progress.setVisibility(VISIBLE);
        setHasOptionsMenu(false);
        fab.hide();
    }

    @Override
    public void hideProgress() {
        progress.setVisibility(GONE);
        if (adapter.hasCities()) {
            setHasOptionsMenu(true);
        }
        needToShowMessage();
        fab.show();
    }

    @Override
    public void setLocation(Location newLocation) {
        hideProgress();
        if (location != null && isLocationEquals(location, newLocation)) {
            mainPresenter.clearCache();
        }
        mainPresenter.loadCity(newLocation);
        this.location = newLocation;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (location != null) {
            mainPresenter.loadCity(location);
        }
    }

    @Override
    public void updateLocation() {
        Snackbar.make(getLayout(), R.string.service_started, Snackbar.LENGTH_SHORT).show();
        getActivity().startService(new Intent(getContext(), LocationService.class));
        showProgress();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_action_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.by_name:
                sortByName();
                return true;
            case R.id.nearest:
                sortRangeAsc();
                return true;
            case R.id.farthest:
                sortRangeDesc();
                return true;
            case R.id.group:
                group();
                return true;
            case R.id.distance:
                new EditDialog(radius -> mainPresenter.setRadius(radius))
                        .show(getFragmentManager(), "EditDialog");
                return true;
            case R.id.google_map:
                navigator.replace(MapsFragment.newInstance(adapter.getCities()));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void sortByName() {
        adapter.sortByName();
    }

    private void sortRangeAsc() {
        adapter.sortRangeAsc();
    }

    private void sortRangeDesc() {
        adapter.sortRangeDesc();
    }

    private void group() {
        adapter.group();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (location != null) {
            outState.putParcelable(LOCATION_BUNDLE, location);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mainPresenter.destroy();
    }
}