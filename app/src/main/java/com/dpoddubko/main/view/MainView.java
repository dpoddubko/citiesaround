package com.dpoddubko.main.view;

import android.content.Context;

import com.dpoddubko.main.interactor.model.City;

import java.util.List;

public interface MainView {
    void updateCity(List<City> data);
    Context getViewContext();
    void  showProgress();
    void hideProgress();
    void updateLocation();
}
