package com.dpoddubko.main.di;

import com.dpoddubko.main.adapter.interactor.AdapterInteractorImpl;
import com.dpoddubko.main.adapter.interactor.IAdapterInteractor;
import com.dpoddubko.main.adapter.presenter.AdapterPresenterImpl;
import com.dpoddubko.main.adapter.presenter.IAdapterPresenter;
import com.dpoddubko.main.interactor.MainInteractor;
import com.dpoddubko.main.interactor.MainInteractorImpl;
import com.dpoddubko.main.presenter.MainPresenter;
import com.dpoddubko.main.presenter.MainPresenterImpl;
import com.dpoddubko.network.ResponseCacheService;
import com.dpoddubko.network.cities.CitiesWebService;
import com.dpoddubko.network.temperature.TemperatureWebService;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {
    @Provides
    MainInteractor provideInteractor(CitiesWebService citiesService) {
        return new MainInteractorImpl(citiesService);
    }

    @Provides
    MainPresenter providePresenter(MainInteractor mainInteractor, ResponseCacheService service) {
        return new MainPresenterImpl(mainInteractor, service);
    }

    @Provides
    IAdapterInteractor provideAdapterInteractor(TemperatureWebService temperatureWebService) {
        return new AdapterInteractorImpl(temperatureWebService);
    }

    @Provides
    IAdapterPresenter provideAdapterPresenter(IAdapterInteractor cityInteractor) {
        return new AdapterPresenterImpl(cityInteractor);
    }
}
