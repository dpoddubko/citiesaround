package com.dpoddubko.main.di;

import com.dpoddubko.main.MainActivity;
import com.dpoddubko.main.adapter.view.MainAdapter;
import com.dpoddubko.main.view.MainFragment;

import dagger.Subcomponent;

@Subcomponent(modules = {MainModule.class})

public interface MainComponent {
    void inject(MainActivity target);
    void inject(MainFragment target);
    void inject(MainAdapter.ViewHolder target);
}
