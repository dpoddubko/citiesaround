package com.dpoddubko.main.adapter.view;

import android.content.res.Resources;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dpoddubko.R;
import com.dpoddubko.app.App;
import com.dpoddubko.main.adapter.presenter.IAdapterPresenter;
import com.dpoddubko.main.interactor.model.City;
import com.dpoddubko.main.interactor.model.DistanceType;

import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static com.dpoddubko.utils.TemperatureUtils.CELSIUS_DEGREE;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private List<City> cities;
    private static State state = State.SORT;

    public static class ViewHolder extends RecyclerView.ViewHolder implements IAdapterView {

        @BindView(R.id.city_name)
        TextView name;
        @BindView(R.id.city_distance)
        TextView distance;
        @BindView(R.id.city_temperature)
        TextView temperature;
        @BindView(R.id.group_title_text)
        TextView titleText;
        @BindView(R.id.group_title_card)
        CardView titleCard;
        @BindView(R.id.main_progress_bar)
        ProgressBar progress;
        private City city;

        @Inject
        IAdapterPresenter presenter;

        private ViewHolder(final View v) {
            super(v);
            ButterKnife.bind(this, v);
            ((App) (v.getContext()).getApplicationContext())
                    .createMainComponent().inject(this);
            presenter.setView(this);

        }

        private void bind(City data) {
            this.city = data;
            this.name.setText(city.getName());
            Resources res = this.itemView.getContext().getResources();
            this.distance.setText(String.format(res.getString(R.string.km), city.getDistance()));
            if (city.isFirst() && state.equals(State.GROUP)) {
                titleCard.setVisibility(VISIBLE);
                titleText.setText(city.getDistanceType().typeName);

            } else {
                titleCard.setVisibility(GONE);
            }

            if (StringUtils.isBlank(city.getTemperature())) {
                presenter.loadTemp(city.getLat(), city.getLng());
            } else {
                setTemperature(city.getTemperature());
                hideProgress();
            }
        }

        @Override
        public void showProgress() {
            progress.setVisibility(VISIBLE);
            temperature.setVisibility(GONE);
        }

        @Override
        public void hideProgress() {
            progress.setVisibility(GONE);
            temperature.setVisibility(VISIBLE);
        }

        @Override
        public void setTemperature(String temp) {
            city.setTemperature(temp);
            temperature.setText(String.format("%s %s", city.getTemperature(), CELSIUS_DEGREE));
        }
    }

    public MainAdapter(List<City> cities) {
        this.cities = cities;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.city_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(cities.get(position));
    }

    @Override
    public int getItemCount() {
        return cities.size();
    }

    public void updateList(List<City> cities) {
        if (cities.size() != this.cities.size() || !this.cities.containsAll(cities)) {
            this.cities = cities;
            notifyDataSetChanged();
        }
    }

    public void sortByName() {
        Collections.sort(cities, (o1, o2) -> o1.getName().compareTo(o2.getName()));
        state = State.SORT;
        notifyDataSetChanged();
    }

    public void sortRangeAsc() {
        Collections.sort(cities, (c1, c2) -> Double.compare(c1.getDistance(), c2.getDistance()));
        state = State.SORT;
        notifyDataSetChanged();
    }

    public void sortRangeDesc() {
        Collections.sort(cities, (c1, c2) -> Double.compare(c2.getDistance(), c1.getDistance()));
        state = State.SORT;
        notifyDataSetChanged();
    }

    public void group() {
        if (cities == null || cities.isEmpty()) return;
        sortRangeAsc();
        setDistanceType();
        setFirstInGroup();
        state = State.GROUP;
        notifyDataSetChanged();
    }

    private void setDistanceType() {
        for (City city : cities) {
            double distance = city.getDistance();
            if (distance < 3) {
                city.setDistanceType(DistanceType.NEAR);
            } else if (distance > 3 && distance < 9) {
                city.setDistanceType(DistanceType.FAR);
            } else if (distance >= 9) {
                city.setDistanceType(DistanceType.VERY_FAR);
            }
        }
    }

    private void setFirstInGroup() {
        City previous = null;
        for (City city : cities) {
            if (previous == null || !city.getDistanceType().equals(previous.getDistanceType())) {
                city.setFirst(true);
                previous = city;
            }
        }
    }

    enum State {
        SORT, GROUP
    }

    public List<City> getCities() {
        return cities;
    }

    public boolean hasCities() {
        return cities != null && !cities.isEmpty();
    }

}

