package com.dpoddubko.main.adapter.interactor;

import com.dpoddubko.main.adapter.interactor.model.Temp;

import io.reactivex.Observable;

public interface IAdapterInteractor {
    Observable<Temp> getTemp(double lat, double lng);
}
