package com.dpoddubko.main.adapter.view;

public interface IAdapterView {

    void showProgress();

    void hideProgress();

    void setTemperature(String temp);
}
