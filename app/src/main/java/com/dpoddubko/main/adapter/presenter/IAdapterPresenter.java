package com.dpoddubko.main.adapter.presenter;

import com.dpoddubko.main.adapter.view.IAdapterView;

public interface IAdapterPresenter {

    void loadTemp(double lat, double lng);

    void setView(IAdapterView view);

    void destroy();

}
