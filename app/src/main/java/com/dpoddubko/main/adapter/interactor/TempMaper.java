package com.dpoddubko.main.adapter.interactor;

import com.dpoddubko.main.adapter.interactor.model.Temp;
import com.dpoddubko.network.temperature.model.TempData;
import com.dpoddubko.utils.TemperatureUtils;

public class TempMaper {

    static Temp transform(TempData tempData) {
        return new Temp(TemperatureUtils.toCelsius(tempData.getMain().getTemp()));
    }
}