package com.dpoddubko.main.adapter.interactor;

import com.dpoddubko.main.adapter.interactor.model.Temp;
import com.dpoddubko.network.temperature.TemperatureWebService;

import io.reactivex.Observable;

public class AdapterInteractorImpl implements IAdapterInteractor {

    private TemperatureWebService tempService;

    public AdapterInteractorImpl(TemperatureWebService tempService) {
        this.tempService = tempService;
    }

    @Override
    public Observable<Temp> getTemp(double lat, double lng) {
        return tempService.getTemp(lat, lng)
                .map(TempMaper::transform);
    }
}
