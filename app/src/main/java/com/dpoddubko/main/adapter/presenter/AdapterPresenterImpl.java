package com.dpoddubko.main.adapter.presenter;

import com.dpoddubko.main.adapter.interactor.IAdapterInteractor;
import com.dpoddubko.main.adapter.view.IAdapterView;
import com.dpoddubko.utils.RxUtils;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class AdapterPresenterImpl implements IAdapterPresenter {

    private IAdapterInteractor cityInteractor;
    private IAdapterView view;

    public AdapterPresenterImpl(IAdapterInteractor cityInteractor) {
        this.cityInteractor = cityInteractor;
    }

    @Override
    public void loadTemp(final double lat, final double lng) {
        view.showProgress();
        RxUtils.add(cityInteractor.getTemp(lat, lng)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(temp -> {
                    view.hideProgress();
                    view.setTemperature(temp.temp);
                }, t -> view.hideProgress()));
    }

    @Override
    public void setView(IAdapterView view) {
        this.view = view;
    }

    @Override
    public void destroy() {
        view = null;
        RxUtils.dispose();
    }
}
