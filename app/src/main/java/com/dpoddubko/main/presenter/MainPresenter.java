package com.dpoddubko.main.presenter;

import android.location.Location;

import com.dpoddubko.main.view.MainView;

public interface MainPresenter {

    void loadCity(Location location);

    void setView(MainView view);

    void destroy();

    void setRadius(int radius);

    void clearCache();

}
