package com.dpoddubko.main.presenter;

import android.content.Context;
import android.location.Location;

import com.dpoddubko.R;
import com.dpoddubko.main.interactor.MainInteractor;
import com.dpoddubko.main.interactor.model.City;
import com.dpoddubko.main.view.MainView;
import com.dpoddubko.network.ResponseCacheService;
import com.dpoddubko.utils.RxUtils;

import java.util.List;

import io.reactivex.Single;

import static com.dpoddubko.main.dialogs.DialogsKt.showDialog;

public class MainPresenterImpl implements MainPresenter {
    private MainView view;
    private MainInteractor mainInteractor;
    private ResponseCacheService cacheService;
    private int radius;

    public MainPresenterImpl(MainInteractor mainInteractor, ResponseCacheService service) {
        this.mainInteractor = mainInteractor;
        this.cacheService = service;
        this.radius = 20;
    }

    @Override
    public void loadCity(Location location) {
        view.showProgress();
        Single<List<City>> preparedObservable = (Single<List<City>>) cacheService
                .getPreparedObservable(mainInteractor.getCities(location.getLatitude(),
                        location.getLongitude(), radius),
                        City.class, true, true);

        RxUtils.add(preparedObservable.subscribe(data -> {
            System.out.println(data);
            view.updateCity(data);
            view.hideProgress();
        }, t -> {
            view.hideProgress();
            Context context = view.getViewContext();
            showDialog(context,
                    context.getString(R.string.exception),
                    context.getString(R.string.invalid_data_msg));
        }));
    }

    @Override
    public void setView(MainView view) {
        this.view = view;
    }

    @Override
    public void destroy() {
        view = null;
        RxUtils.dispose();
    }

    @Override
    public void setRadius(int radius) {
        this.radius = radius;
        cacheService.clearCache();
        view.updateLocation();
    }

    @Override
    public void clearCache() {
        cacheService.clearCache();
    }
}
