package com.dpoddubko.map;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.dpoddubko.R;
import com.dpoddubko.main.interactor.model.City;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import static com.dpoddubko.utils.TemperatureUtils.CELSIUS_DEGREE;

public class MapsFragment extends Fragment implements OnMapReadyCallback {
    private static final String ARG_CITY_LIST = "arg_city_list";
    private List<City> cities;
    private GoogleMap map;

    public MapsFragment() {
        // Required empty public constructor
    }

    public static MapsFragment newInstance(List<City> cities) {
        MapsFragment fragment = new MapsFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList(ARG_CITY_LIST, (ArrayList<City>) cities);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            cities = getArguments().getParcelableArrayList(ARG_CITY_LIST);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_map, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        if (cities != null && !cities.isEmpty())
            for (City c : cities) {
                map = googleMap;
                LatLng latLng = new LatLng(c.getLat(), c.getLng());
                map.addMarker(new MarkerOptions()
                        .position(latLng)
                        .title(c.getName())
                        .snippet(String.format("temperature: %s %s", c.getTemperature(),
                                CELSIUS_DEGREE)));
                map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(latLng)
                        .zoom(9.5f).build();
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            }
        else {
            Toast.makeText(getContext(), getString(R.string.list_empty), Toast.LENGTH_SHORT).show();
        }
    }
}