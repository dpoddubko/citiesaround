package com.dpoddubko.utils;

import android.location.Location;

public class LocationUtils {

    private LocationUtils() {
        //hidden
    }

    public static boolean isLocationEquals(Location oldLocation, Location newLocation) {
        return oldLocation.getLatitude() == newLocation.getLatitude() &&
                oldLocation.getLongitude() == newLocation.getLongitude();
    }
}