package com.dpoddubko.utils;

public interface Constants {

    String LOCATION_CHANGED_ACTION = "location-changed-action";
    String LOCATION_EXTRA = "location";
    String ERROR_MSG_EXTRA = "error_msg_extra";
    String ERROR_ACTION = "error_action";
}
