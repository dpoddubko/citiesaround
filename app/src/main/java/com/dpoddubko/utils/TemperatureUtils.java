package com.dpoddubko.utils;

import android.annotation.SuppressLint;

public class TemperatureUtils {

    public static final String CELSIUS_DEGREE = "\u00b0" + "C";

    private TemperatureUtils() {
        //hidden
    }

    @SuppressLint("DefaultLocale")
    public static String toCelsius(String kelvin) {
        double kelvinDouble = Double.parseDouble(kelvin);
        return String.format("%.2f", (kelvinDouble - 273.15));
    }
}
